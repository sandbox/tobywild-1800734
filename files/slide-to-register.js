/*!
 * Slider control v1.0a1 / Commande de barre coulissante v1.0a1
 * Web Experience Toolkit (WET) / Boîte à outils de l'expérience Web (BOEW)
 * Terms and conditions of use: http://tbs-sct.ircan.gc.ca/projects/gcwwwtemplates/wiki/Terms
 * Conditions régissant l'utilisation : http://tbs-sct.ircan.gc.ca/projects/gcwwwtemplates/wiki/Conditions
 */	

var PE = {
    progress: function(props){
		PE.parameters = props /** DEPRECATED: Backward Compatibility **/ ;
    },

	load_ui: function(themeenabled){
		// stop the ui from being loaded more than once
		if (PE.uiloaded){ return true };
		var use_theme = (typeof(themeenabled)!='undefined') ? themeenabled : "use-theme";
		PE.uiloaded = true;
	}
};

var sliderWET = {
	init : function(){},
	createHandler : function(event, ui){
		sliderWET.displayStatus(event, ui);
	},
	startHandler : function(event, ui){
		sliderWET.displayStatus(event, ui);
	},
	slideHandler : function(event, ui){
		sliderWET.displayStatus(event, ui);
	},
	changeHandler : function(event, ui){
		sliderWET.displayStatus(event, ui);
	},
	stopHandler : function(event, ui){
		sliderWET.displayStatus(event, ui);
		
		/* When slider reaches 100%, remove the 'disabled' attribute on target.  Used to enable an input button */
		if (ui.value == 100) {
			$("#edit-submit").removeAttr("disabled");
			$("#edit-submit").attr("value","Submit");
			$("#edit-slidefield").val("Unlocked"); /* Validation Term */
		} else {
			$("#edit-submit").attr("disabled","disabled");
			$("#edit-submit").attr("value","Slide to 100% to enable");
			$("#edit-slidefield").val("Locked");
		}
	},
	displayStatus : function(event, ui) {
		$("#" + $(event.target).attr("id") + "-event").text(event.type);
		$("#" + $(event.target).attr("id") + "-value").text(ui.value);
	}
}

/* loading jQuery UI */
PE.load_ui();

/* plugin init */
jQuery(document).ready(function() {
	sliderWET.init();
	$( ".slider" ).slider({
		create: sliderWET.createHandler,
		start: sliderWET.startHandler,
		slide: sliderWET.slideHandler,
		change: sliderWET.changeHandler,
		stop: sliderWET.stopHandler
	});
});